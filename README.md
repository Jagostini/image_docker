# Image Docker

Ce projet permet de récupérer les images docker sans passer par le dépôt officiel de docker.

## Instructions

Commencer par récupérer le projet avec la commande `git clone git@gitlab.com:Jagostini/image_docker.git`

Puis pour charger vos images avec la commande `docker image load --input imagename_tag.tar`

Vérifier la présence de vos images avec `docker image ls`

